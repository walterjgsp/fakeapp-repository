package com.wcode.fakeapp.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.fakesdk.Classifier

class MainViewModel : ViewModel() {


    private val inputEntry: MutableLiveData<String> = MutableLiveData()

    fun applySDK(input: String) {
        inputEntry.value = Classifier.helloSDK(input)
    }

    fun getEntry() = inputEntry
}