package com.wcode.fakeapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.brightinsight.nn.iprimeapp.utils.observeOnce
import com.wcode.fakeapp.ui.MainViewModel
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class MainViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    lateinit var mainViewModel: MainViewModel

    @Before
    fun setUp() {
        mainViewModel = MainViewModel()
    }

    /**
     * Test if the classification is done as expected
     */
    @Test
    fun classifyValueTest() {
        val testValue = "jakdmskamkosa"
        mainViewModel.applySDK(testValue)
        mainViewModel.getEntry().observeOnce { output ->
            assertEquals(testValue.toUpperCase(),output)
        }
    }
}