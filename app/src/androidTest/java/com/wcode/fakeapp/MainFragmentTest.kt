package com.wcode.fakeapp

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainFragmentTest {

    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testInitialViewState() {
        onView(withId(R.id.textView_mainFragment))
            .check(matches(withText("Loading ...")))
    }

    @Test
    fun testClassifier(){
        val testValue = "jakdmskamkosa"
        onView(withId(R.id.editText_input))
            .perform(typeText(testValue),closeSoftKeyboard())

        onView(withId(R.id.button_mainFragment))
            .perform(click())

        onView(withId(R.id.textView_mainFragment))
            .check(matches(withText(testValue.toUpperCase())))
    }
}